variable "namespace" {
  description = "Namespace to deploy to."
  nullable    = false
  type        = string
  default     = "default"
}

variable "nfs_media_host" {
  description = "Host that provides NFS for media storage."
  nullable    = false
  type        = string
}

variable "nfs_media_path" {
  description = "Path to NFS media mount."
  nullable    = false
  type        = string
}

variable "local_domain" {
  description = "Local domain to create records under"
  nullable    = false
  type        = string
}
