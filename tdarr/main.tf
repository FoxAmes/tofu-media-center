terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
}

resource "kubernetes_persistent_volume_claim" "tdarr_configs" {
  metadata {
    name      = "tdarr-configs"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" : "256Mi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "tdarr_server" {
  metadata {
    name = "tdarr-server"
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" : "2Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "tdarr" {
  metadata {
    name = "tdarr"
    namespace = var.namespace
  }
  spec {
    selector {
      match_labels = { "app" : "tdarr" }
    }
    template {
      metadata {
        name = "tdarr"
        labels = {
          "app" : "tdarr"
        }
      }
      spec {
        volume {
          name = kubernetes_persistent_volume_claim.tdarr_configs.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.tdarr_configs.metadata[0].name
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.tdarr_server.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.tdarr_server.metadata[0].name
          }
        }
        volume {
          name = "tdarr-media"
          nfs {
            server = var.nfs_media_host
            path   = var.nfs_media_path
          }
        }
        container {
          name  = "tdarr"
          image = "ghcr.io/haveagitgat/tdarr"
          env {
            name = "serverIP"
            value = "0.0.0.0"
          }
          env {
            name = "serverPort"
            value = "8266"
          }
          env {
            name = "webUIPort"
            value = "8265"
          }
          env {
            name = "internalNode"
            value = "false"
          }
          env {
            name = "inContainer"
            value = "true"
          }
          env {
            name = "ffmpegVersion"
            value = "6"
          }
          env {
            name = "TZ"
            value = "America/Denver"
          }
          env {
            name = "PUID"
            value = "1000"
          }
          env {
            name = "PGID"
            value = "1000"
          }
          volume_mount {
            mount_path = "/app/configs"
            name       = kubernetes_persistent_volume_claim.tdarr_configs.metadata[0].name
          }
          volume_mount {
            mount_path = "/app/server"
            name       = kubernetes_persistent_volume_claim.tdarr_server.metadata[0].name
          }
          volume_mount {
            mount_path = "/temp"
            name       = "tdarr-media"
            sub_path = "transcodes"
          }
          volume_mount {
            mount_path = "/media"
            name       = "tdarr-media"
          }
          port {
            name           = "tdarr-http"
            container_port = 8265
            protocol       = "TCP"
          }
          port {
            name           = "tdarr-server"
            container_port = 8266
            protocol       = "UDP"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "tdarr_web" {
  metadata {
    name      = "tdarr-web"
    namespace = "default"
  }
  spec {
    type = "LoadBalancer"
    port {
      name        = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[0].name
      target_port = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[0].container_port
      port        = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[0].container_port
    }
    selector = { "app" = "tdarr" }
  }
}

resource "kubernetes_service" "tdarr_server" {
  metadata {
    name      = "tdarr-server"
    namespace = "default"
  }
  spec {
    type = "LoadBalancer"
    port {
      name        = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[1].name
      target_port = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[1].container_port
      port        = kubernetes_deployment.tdarr.spec[0].template[0].spec[0].container[0].port[1].container_port
    }
    selector = { "app" = "tdarr" }
  }
}

resource "kubernetes_manifest" "tdarr_ingress_route_http" {
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "tdarr-web-http"
      "namespace" : "default"
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`tdarr.${var.local_domain}`)"
          "priority" : "10"
          "services" : [
            {
              "name" : kubernetes_service.tdarr_web.metadata[0].name
              "namespace" : kubernetes_service.tdarr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.tdarr_web.spec[0].port[0].port
              "responseForwarding" : {
                "flushInterval" : "1ms"
              }
            }
          ]
        }
      ]
    }
  }
}
