variable "namespace" {
  description = "Namespace to deploy to."
  nullable    = false
  type        = string
  default     = "default"
}

variable "nfs_media_host" {
  description = "Host that provides NFS for media storage."
  nullable    = false
  type        = string
}

variable "nfs_media_path" {
  description = "Path to NFS media mount."
  nullable    = false
  type        = string
}

variable "nfs_downloads_path" {
  description = "Path to NFS downloads mount."
  nullable    = false
  type        = string
}

variable "cert_email" {
  description = "Email to use when registering a Let's Encrypt cert"
  nullable    = false
  type        = string
}

variable "cert_organization" {
  description = "Organization to use when registering a Let's Encrypt cert"
  nullable    = false
  type        = string
}

variable "local_domain" {
  description = "Local domain to create records under"
  nullable    = false
  type        = string
  default     = "media.lan"
}

variable "local_dns_ip" {
  description = "IP address for DNS requests that CoreDNS can respond on"
  nullable    = false
  type        = string
}

variable "subdomains" {
  description = "List of subdomains to create records for"
  nullable    = false
  type        = list(string)
  default = [
    "stream",
    "seerr",
    "sonarr",
    "radarr",
    "lidarr",
    "bazarr",
    "prowlarr",
    "nzb",
    "seedbox",
  ]
}

variable "public_domain" {
  description = "Publicly registered domain to create records under and acquire SSL certs for (optional)"
  nullable    = true
  type        = string
  default     = null
}

variable "wireguard_egress_config" {
  description = "Wireguard configuration file for traffic egress"
  nullable    = false
  type        = string
}

variable "sonarr_api_key" {
  description = "API key generated in Sonarr for internal services to connect with"
  nullable    = true
  type        = string
}

variable "use_jellyfin_mpp" {
  description = "Use jellyfin-mpp, a fork of jellyfin with support for Rockchip's MPP video acceleration"
  nullable    = false
  type        = bool
  default     = false
}
