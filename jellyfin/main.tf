terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
}

resource "kubernetes_persistent_volume_claim" "jellyfin_config" {
  metadata {
    name      = "jellyfin-config"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" : "4Gi"
      }
    }
  }
  # lifecycle {
  #   ignore_changes = [
  #     spec[0].resources[0].requests
  #   ]
  # }
}

resource "kubernetes_persistent_volume_claim" "jellyfin_cache" {
  metadata {
    name = "jellyfin-cache"
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" : "2Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "jellyfin" {
  count = var.use_jellyfin_mpp ? 0 : 1
  metadata {
    name = "jellyfin"
  }
  spec {
    selector {
      match_labels = { "app" : "jellyfin" }
    }
    template {
      metadata {
        name = "jellyfin"
        labels = {
          "app" : "jellyfin"
        }
      }
      spec {
        volume {
          name = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          }
        }
        volume {
          name = "jellyfin-media"
          nfs {
            server = var.nfs_media_host
            path   = var.nfs_media_path
          }
        }
        volume {
          name = "jellyfin-transcodes"
          empty_dir {}
        }
        container {
          name  = "jellyfin"
          image = "jellyfin/jellyfin:latest"
          volume_mount {
            mount_path = "/config"
            name       = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          }
          volume_mount {
            mount_path = "/config/transcodes"
            name       = "jellyfin-transcodes"
          }
          volume_mount {
            mount_path = "/cache"
            name       = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          }
          volume_mount {
            mount_path = "/media"
            name       = "jellyfin-media"
          }
          port {
            name           = "jellyfin-http"
            container_port = 8096
            protocol       = "TCP"
          }
          port {
            name           = "jellyfin-disc"
            container_port = 1900
            protocol       = "UDP"
          }
          port {
            name           = "jellyfin-whois"
            container_port = 7359
            protocol       = "UDP"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "jellyfin_mpp" {
  count = var.use_jellyfin_mpp ? 1 : 0
  metadata {
    name = "jellyfin"
  }
  spec {
    replicas = 1
    selector {
      match_labels = { "app" : "jellyfin" }
    }
    template {
      metadata {
        name = "jellyfin"
        labels = {
          "app" : "jellyfin"
        }
      }
      spec {
        volume {
          name = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          }
        }
        volume {
          name = "jellyfin-media"
          nfs {
            server = var.nfs_media_host
            path   = var.nfs_media_path
          }
        }
        volume {
          name = "jellyfin-transcodes"
          empty_dir {}
        }
        dynamic "volume" {
          for_each = ["iep", "rga", "dri", "dma_heap", "mpp_service", "mpp-service", "vpu_service", "vpu-service", "hevc_service", "hevc-service", "rkvdec", "rkvenc", "avsd", "vepu", "h265e"]
          iterator = dev
          content {
            name = "jellyfin-dev-${replace(dev.value, "_", "--")}"
            host_path {
              path = "/dev/${dev.value}"
            }
          }
        }
        container {
          name  = "jellyfin"
          image = "jjm2473/jellyfin-mpp:latest"
          security_context {
            privileged = true # required for hardware access
          }
          dynamic "volume_mount" {
            for_each = ["iep", "rga", "dri", "dma_heap", "mpp_service", "mpp-service", "vpu_service", "vpu-service", "hevc_service", "hevc-service", "rkvdec", "rkvenc", "avsd", "vepu", "h265e"]
            iterator = dev
            content {
              name       = "jellyfin-dev-${replace(dev.value, "_", "--")}"
              mount_path = "/dev/${dev.value}"
            }
          }
          volume_mount {
            mount_path = "/config"
            name       = kubernetes_persistent_volume_claim.jellyfin_config.metadata[0].name
          }
          volume_mount {
            mount_path = "/config/transcodes"
            name       = "jellyfin-transcodes"
          }
          volume_mount {
            mount_path = "/cache"
            name       = kubernetes_persistent_volume_claim.jellyfin_cache.metadata[0].name
          }
          volume_mount {
            mount_path = "/media"
            name       = "jellyfin-media"
          }
          port {
            name           = "jellyfin-http"
            container_port = 8096
            protocol       = "TCP"
          }
          port {
            name           = "jellyfin-disc"
            container_port = 1900
            protocol       = "UDP"
          }
          port {
            name           = "jellyfin-whois"
            container_port = 7359
            protocol       = "UDP"
          }
        }
        node_selector = { "video-encoding" : "mpp" }
      }
    }
  }
}

resource "kubernetes_service" "jellyfin_web" {
  metadata {
    name      = "jellyfin-web"
    namespace = "default"
  }
  spec {
    type = "LoadBalancer"
    port {
      name        = "jellyfin-https"
      target_port = 8096
      port        = 8096
    }
    selector = { "app" = "jellyfin" }
  }
}

resource "kubernetes_manifest" "jellyfin_tls" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "cert-manager.io/v1"
    "kind" : "Certificate"
    "metadata" : {
      "name" : "jellyfin-tls"
      "namespace" : "default"
    }
    "spec" : {
      "secretName" : "jellyfin-tls"
      "duration" : "2160h0m0s"   # 90d
      "renewBefore" : "360h0m0s" # 15d
      "subject" : {
        "organizations" : [
          var.cert_organization
        ]
      }
      "commonName" : "stream.${var.public_domain}"
      "privateKey" : {
        "algorithm" : "RSA"
        "encoding" : "PKCS1"
        "size" : "2048"
      }
      "usages" : [
        "server auth",
        "client auth"
      ]
      "dnsNames" : [
        "stream.${var.public_domain}"
      ]
      "issuerRef" : {
        "name" : "letsencrypt-prod"
        "kind" : "ClusterIssuer"
      }
    }
  }
}

resource "kubernetes_manifest" "jellyfin_https_redirect" {
  manifest = {
    "apiVersion": "traefik.containo.us/v1alpha1"
    "kind": "Middleware"
    "metadata": {
      "name": "jellyfin-https-redirect"
      "namespace" : var.namespace
    }
    "spec": {
      "redirectScheme": {
        "scheme": "https"
        "port": "443"
        "permanent": "true"
      }
    }
  }
}

resource "kubernetes_manifest" "jellyfin_ingress_route_http" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "jellyfin-web-http"
      "namespace" : "default"
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`stream.${var.public_domain}`)"
          "priority" : "10"
          "middlewares" : [
            {
              "name": kubernetes_manifest.jellyfin_https_redirect.manifest["metadata"]["name"]
              "namespace": kubernetes_manifest.jellyfin_https_redirect.manifest["metadata"]["namespace"]
            }
          ]
          "services" : [
            {
              "name" : kubernetes_service.jellyfin_web.metadata[0].name
              "namespace" : kubernetes_service.jellyfin_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.jellyfin_web.spec[0].port[0].port
              "responseForwarding" : {
                "flushInterval" : "1ms"
              }
            }
          ]
        }
      ]
    }
  }
}

resource "kubernetes_manifest" "jellyfin_ingress_route" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "jellyfin-web"
      "namespace" : "default"
    }
    "spec" : {
      "entryPoints" : [
        "websecure"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`stream.${var.public_domain}`)"
          "priority" : "10"
          "services" : [
            {
              "name" : kubernetes_service.jellyfin_web.metadata[0].name
              "namespace" : kubernetes_service.jellyfin_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.jellyfin_web.spec[0].port[0].port
              "responseForwarding" : {
                "flushInterval" : "1ms"
              }
            }
          ]
        }
      ]
      "tls" : {
        "secretName" : kubernetes_manifest.jellyfin_tls[0].manifest["spec"]["secretName"]
        "domains" : [
          {
            "main" : kubernetes_manifest.jellyfin_tls[0].manifest["spec"]["commonName"]
          }
        ]
      }
    }
  }
}
