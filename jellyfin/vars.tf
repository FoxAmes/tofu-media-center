variable "namespace" {
  description = "Namespace to deploy to."
  nullable    = false
  type        = string
  default     = "default"
}

variable "nfs_media_host" {
  description = "Host that provides NFS for media storage."
  nullable    = false
  type        = string
}

variable "nfs_media_path" {
  description = "Path to NFS media mount."
  nullable    = false
  type        = string
}

variable "cert_organization" {
  description = "Organization to use when registering a Let's Encrypt cert"
  nullable    = false
  type        = string
}

variable "public_domain" {
  description = "Publicly registered domain to create records under and acquire SSL certs for (optional)"
  nullable    = true
  type        = string
}

variable "use_jellyfin_mpp" {
  description = "Use jellyfin-mpp, a fork of jellyfin with support for Rockchip's MPP video acceleration"
  nullable    = false
  type        = bool
  default     = false
}
