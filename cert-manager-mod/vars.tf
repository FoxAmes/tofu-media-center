variable "namespace" {
  description = "Namespace to deploy to."
  nullable    = false
  type        = string
}

variable "cert_email" {
  description = "Email to use when registering a Let's Encrypt cert"
  nullable    = false
  type        = string
}
