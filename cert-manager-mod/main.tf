terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
}

resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  create_namespace = "true"
  version          = "v1.14.4"
  set {
    name  = "installCRDs"
    value = "true"
  }
  lifecycle {
    ignore_changes = [
      create_namespace
    ]
  }
}

resource "kubernetes_manifest" "le_staging_cluster_issuer" {
  count = 1 # Must be bootstrapped
  manifest = {
    "apiVersion" : "cert-manager.io/v1"
    "kind" : "ClusterIssuer"
    "metadata" : {
      "name" : "letsencrypt-staging"
    }
    "spec" : {
      "acme" : {
        "email" : var.cert_email
        "server" : "https://acme-staging-v02.api.letsencrypt.org/directory"
        "privateKeySecretRef" : {
          "name" : "le-staging-account-key"
        }
        "solvers" : [
          {
            "http01" : {
              "ingress" : {
                "class" : "traefik"
              }
            }
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "le_prod_cluster_issuer" {
  count = 1 # Must be bootstrapped
  manifest = {
    "apiVersion" : "cert-manager.io/v1"
    "kind" : "ClusterIssuer"
    "metadata" : {
      "name" : "letsencrypt-prod"
    }
    "spec" : {
      "acme" : {
        "email" : var.cert_email
        "server" : "https://acme-v02.api.letsencrypt.org/directory"
        "privateKeySecretRef" : {
          "name" : "le-prod-account-key"
        }
        "solvers" : [
          {
            "http01" : {
              "ingress" : {
                "class" : "traefik"
              }
            }
          }
        ]
      }
    }
  }
}
