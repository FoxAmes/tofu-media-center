variable "local_domain" {
  description = "Local domain to create records under"
  nullable    = false
  type        = string
}

variable "local_dns_ip" {
  description = "IP address for DNS requests that CoreDNS can respond on"
  nullable    = false
  type        = string
}

variable "subdomains" {
  description = "List of subdomains to create records for"
  nullable    = false
  type        = list(string)
}
