terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
}

locals {
  coredns_template_vars = {
    "local_domain" : var.local_domain,
    "subdomains" : var.subdomains,
    "local_dns_ip" : var.local_dns_ip
  }
}

resource "kubernetes_config_map" "coredns_custom" {
  metadata {
    name      = "coredns-custom"
    namespace = "kube-system"
  }
  data = {
    "${var.local_domain}.server" = templatefile("${path.module}/coredns-custom/local_domain.server", local.coredns_template_vars),
    "${var.local_domain}.lan"    = templatefile("${path.module}/coredns-custom/local_domain", local.coredns_template_vars),
  }
}
