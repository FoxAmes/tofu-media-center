terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
}

resource "helm_release" "longhorn" {
  name             = "longhorn"
  repository       = "https://charts.longhorn.io"
  chart            = "longhorn"
  namespace        = "longhorn-system"
  create_namespace = "true"
  version          = "1.5.3"
}

resource "kubernetes_manifest" "longhorn_ingress_route" {
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "longhorn-web"
      "namespace" : "longhorn-system"
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`longhorn.${var.local_domain}`)"
          "services" : [
            {
              "name" : "longhorn-frontend"
              "namespace" : "longhorn-system"
              "passHostHeader" : "true"
              "port" : 80
            }
          ]
        },
      ]
    }
  }
}
