variable "local_domain" {
  description = "Local domain to create records under"
  nullable    = false
  type        = string
}
