terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.22.0"
    }
  }
}

module "longhorn" {
  source       = "./longhorn-mod"
  local_domain = var.local_domain
}

module "cert_manager" {
  source     = "./cert-manager-mod"
  cert_email = var.cert_email
  namespace  = var.namespace
}

module "corsair" {
  source                  = "./corsair"
  local_domain            = var.local_domain
  nfs_downloads_path      = var.nfs_downloads_path
  nfs_media_host          = var.nfs_media_host
  nfs_media_path          = var.nfs_media_path
  public_domain           = var.public_domain
  wireguard_egress_config = var.wireguard_egress_config
  namespace               = var.namespace
  cert_organization       = var.cert_organization
  sonarr_api_key          = var.sonarr_api_key
}

module "jellyfin" {
  source            = "./jellyfin"
  nfs_media_host    = var.nfs_media_host
  nfs_media_path    = var.nfs_media_path
  public_domain     = var.public_domain
  cert_organization = var.cert_organization
  use_jellyfin_mpp  = var.use_jellyfin_mpp
}

module "coredns_k3s" {
  source       = "./coredns-k3s"
  local_domain = var.local_domain
  local_dns_ip = var.local_dns_ip
  subdomains   = var.subdomains
}

module "tdarr" {
  source       = "./tdarr"
  nfs_media_host    = var.nfs_media_host
  nfs_media_path    = var.nfs_media_path
  local_domain = var.local_domain
}
