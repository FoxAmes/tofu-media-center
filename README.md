[TOC]

# Forest's Media Center
This is a collection of tools that I have grown to love using to maintain a robust, accessible, and easily usable home media center platform.

## 🚧 Work in Progress 🚧
First, a warning: I am currently using this myself, but as such I designed it with many assumptions about **my** environment. This means that it is not completely modular, platform-independent, or as configurable as I would like it to eventually be. The requirements are therefore subject to change, and somewhat restrictive.

### Currently Inflexible Deployment Components
These components are necessary for the function of this platform, but in many environments may already exist. Ideally, we should allow the user to disable or override these components for alternatives or existing deployments.
- Longhorn (storage provisioner)
- cert-manager (certificate management)
- wireguard VPN (egress)
- Deluge (torrent management)

## Requirements
- OpenTofu (Terraform)
- k3s
  - DNS configuration is specific to extending k3s's built-in CoreDNS deployment
- Traefik (shipped by default on k3s)
- NFS Provider
  - Used for media storage
- VPN with wireguard support
  - Used for split egress of download clients
- A publicly registered domain
  - Let's Encrypt certs are automatically requested via ACME for Jellyfin and Jellyseerr
- Usenet provider *and* indexer (optional)
  - Only necessary if you plan on using Usenet
  - Check out [this Reddit thread](https://www.reddit.com/r/usenet/wiki/index/) for more information

## Components
This media center is comprised of several open-source tools to make management, categorization, viewing, and sharing easier:
- [Servarr suite](https://wiki.servarr.com/) (Curation)
  - [Sonarr](https://wiki.servarr.com/en/sonarr) (TV)
  - [Radarr](https://wiki.servarr.com/en/radarr) (Movies)
  - [Lidarr](https://wiki.servarr.com/en/lidarr) (Music)
  - [Prowlarr](https://wiki.servarr.com/en/prowlarr) (Indexer Management)
- [Bazarr](https://github.com/morpheus65535/bazarr) (Subtitle Management)
- [Jellyfin](https://jellyfin.org/) (Streaming)
- [Jellyseerr](https://github.com/Fallenbagel/jellyseerr) (Request Management)
- [Deluge](https://github.com/deluge-torrent/deluge) (BitTorrent client)
- [SABnzbd](https://github.com/sabnzbd/sabnzbd) (Usenet client)
- [saytrs](https://gitlab.com/FoxAmes/saytrs) (Youtube/Sonarr integration)

## Setup
You'll need a functional k3s cluster to get started, [their quick-start guide](https://docs.k3s.io/quick-start) is a good place to start. This can be a single-node Raspberry Pi or a multi-node cluster with heavy-duty x86 cores.

Once you have k3s set up, you'll need some sort of storage that can be mounted via NFSv4. You could install an NFS server locally, use a NAS device, or whatever makes the most sense in your case.

After you have k3s and NFS set up, simply include this module in your root module, like so:

```tf
module "media-center" {
  source = "git::https://gitlab.com/FoxAmes/tofu-media-center"
  # Be sure to include variables here
}
```

Make sure that you configure both the [`helm`](https://github.com/opentofu/terraform-provider-helm) and [`kubernetes`](https://github.com/opentofu/terraform-provider-kubernetes) providers, which this module depends on.

Check the [`variable file`](vars.tf) to see what all can and must be set, Then simply `tofu apply` and you should be off to the races.

## Accessing the Components
This module will create a CoreDNS zone with records for each component. The default local domain used is `media.lan`, so the DNS records in the default case would be as follows:

- Seerr: `seerr.media.lan`
- Radarr: `radarr.media.lan`
- Lidarr: `lidarr.media.lan`
- Prowlarr: `prowlarr.media.lan`
- Deluge: `seedbox.media.lan`
- SABnzbd: `nzb.media.lan`
- Jellyfin: `stream.media.lan`
- Jellyseerr: `seerr.media.lan`

These records need to point to traefik, which should be accessible on any of your k3s nodes by default. Traefik will automatically route traffic by the hostname of the request to the appropriate services using IngressRoutes created by the module.

## Configuration
At present, configuration does not happen automatically. The order of operations isn't critical, but here's how I set everything up:
1. [Configure your indexers on prowlarr](https://wiki.servarr.com/prowlarr/indexers)
2. [Configure your download clients on prowlarr](https://wiki.servarr.com/prowlarr/settings#download-clients)
3. [Configure Radarr](https://wiki.servarr.com/radarr/quick-start-guide)
  a. Be sure to [copy your API key](https://wiki.servarr.com/radarr/settings#security), we will need this later.
4. [Configure Sonarr](https://wiki.servarr.com/sonarr/quick-start-guide)
  a. Be sure to copy this API key as well.
5. [Configure Lidarr](https://wiki.servarr.com/en/lidarr/quick-start-guide)
  a. Copy this API key too.
6. [Connect Prowlarr to Radarr, Sonarr, and Lidarr](https://wiki.servarr.com/prowlarr/settings#applications)
  a. You can use the DNS records above, and the API keys you took down.
  b. Optionally, you can configure the indexers and download clients on each application individually, if you'd prefer.
7. [Configure Jellyfin](https://jellyfin.org/docs/general/quick-start)
8. [Configure Jellyseerr](https://docs.overseerr.dev/using-overseerr/settings)
  a. This documentation is for Overseerr, but Jellyseerr is nearly identical in usage and configuration.
9. Go back to Radarr, Sonarr, and Lidarr to set up [a Jellyfin connection](https://wiki.servarr.com/radarr/settings#connections) (Optional)
  a. This enables your management tools to inform Jellyfin when they make changes to content, preventing the need to manually rescan your libraries.

## Credits
- [Servarr](https://wiki.servarr.com/) - The servarr suite
- [Jellyfin Team](https://jellyfin.org/docs/general/about/) - Jellyfin
- [Jellyseerr Team](https://github.com/Fallenbagel/jellyseerr#contributors-) - Jellyseerr
- [Contributors to the deluge project](https://github.com/deluge-torrent/deluge/graphs/contributors) - Deluge
- [Contributors to SABnzbd](https://github.com/sabnzbd/sabnzbd/graphs/contributors) - SABnzbd
- Countless other open source projects and their contributors without which none of these projects or this one could exist
