resource "kubernetes_config_map" "wireguard_config" {
  metadata {
    name      = "seedbox-wireguard"
    namespace = var.namespace
  }
  data = {
    "wg0.conf" = var.wireguard_egress_config
  }
}

resource "kubernetes_persistent_volume_claim" "deluge_conf" {
  metadata {
    name      = "deluge-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "256Mi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "sabnzbd_conf" {
  metadata {
    name      = "sabnzbd-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "2Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "seedbox" {
  metadata {
    name      = "seedbox"
    namespace = var.namespace
  }
  spec {
    strategy {
      type = "Recreate"
    }
    replicas = 1
    selector {
      match_labels = {
        "app" : "seedbox"
      }
    }
    template {
      metadata {
        name      = "seedbox"
        namespace = var.namespace
        labels = {
          "app" : "seedbox"
        }
      }
      spec {
        container {
          name  = "vpn"
          image = "linuxserver/wireguard:latest"
          volume_mount {
            mount_path = "/config/wg0.conf"
            sub_path   = "wg0.conf"
            name       = "wireguard-conf"
          }
          security_context {
            capabilities {
              add = [
                "NET_ADMIN",
                "SYS_MODULE"
              ]
            }
          }
        }
        security_context {
          sysctl {
            name  = "net.ipv4.conf.all.src_valid_mark"
            value = "1"
          }
        }
        volume {
          name = "wireguard-conf"
          config_map {
            name = "seedbox-wireguard"
          }
        }
        volume {
          name = "nas-downloads"
          nfs {
            server = var.nfs_media_host
            path   = var.nfs_downloads_path
          }
        }
        volume {
          name = "nas-tv"
          nfs {
            server = var.nfs_media_host
            path   = "${var.nfs_media_path}/TV"
          }
        }
        volume {
          name = "nas-movies"
          nfs {
            server = var.nfs_media_host
            path   = "${var.nfs_media_path}/Movies"
          }
        }
        container {
          name  = "deluge"
          image = "linuxserver/deluge:latest"
          port {
            container_port = 8112
            protocol       = "TCP"
          }
          volume_mount {
            name       = "deluge-conf"
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-downloads"
            mount_path = "/downloads"
          }
          volume_mount {
            name       = "nas-tv"
            mount_path = "/tv"
          }
          volume_mount {
            name       = "nas-movies"
            mount_path = "/movies"
          }
        }
        volume {
          name = "deluge-conf"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.deluge_conf.metadata[0].name
          }
        }
        container {
          name  = "sabnzbd"
          image = "linuxserver/sabnzbd:latest"
          port {
            container_port = 8081
            protocol       = "TCP"
          }
          volume_mount {
            name       = "sabnzbd-conf"
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-downloads"
            mount_path = "/downloads"
          }
        }
        volume {
          name = "sabnzbd-conf"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.sabnzbd_conf.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "seedbox_web" {
  metadata {
    name      = "seedbox-web"
    namespace = var.namespace
  }
  spec {
    type = "ClusterIP"
    selector = {
      "app" : "seedbox"
    }
    port {
      name        = "seedbox-web"
      port        = 8112
      target_port = 8112
    }
    port {
      name        = "sabnzbd-web"
      port        = 8081
      target_port = 8081
    }
  }
}

resource "kubernetes_manifest" "seedbox_ingress_route" {
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "seedbox-web"
      "namespace" : var.namespace
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`seedbox.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.seedbox_web.metadata[0].name
              "namespace" : kubernetes_service.seedbox_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.seedbox_web.spec[0].port[0].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`nzb.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.seedbox_web.metadata[0].name
              "namespace" : kubernetes_service.seedbox_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.seedbox_web.spec[0].port[1].port
            }
          ]
        },
      ]
    }
  }
}

