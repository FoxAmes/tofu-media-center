locals {
  saytrs_conf_template = {
    "sonarr_api_key" : var.sonarr_api_key
  }
}

resource "kubernetes_persistent_volume_claim" "seerr_conf" {
  metadata {
    name      = "seerr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "256Mi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "radarr_conf" {
  metadata {
    name      = "radarr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "4Gi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "sonarr_conf" {
  metadata {
    name      = "sonarr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "4Gi"
      }
    }
  }
}

resource "kubernetes_config_map" "saytrs_conf" {
  metadata {
    name      = "saytrs-conf"
    namespace = var.namespace
  }
  data = {
    "config.toml" = templatefile("${path.module}/saytrs.toml", local.saytrs_conf_template)
  }
}

resource "kubernetes_persistent_volume_claim" "prowlarr_conf" {
  metadata {
    name      = "prowlarr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "1Gi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "lidarr_conf" {
  metadata {
    name      = "lidarr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "2Gi"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "bazarr_conf" {
  metadata {
    name      = "bazarr-conf"
    namespace = var.namespace
  }
  spec {
    access_modes = [
      "ReadWriteOnce"
    ]
    storage_class_name = "longhorn"
    resources {
      requests = {
        "storage" = "2Gi"
      }
    }
  }
}

resource "kubernetes_deployment" "arr" {
  metadata {
    name      = "arr"
    namespace = var.namespace
  }
  spec {
    strategy {
      type = "Recreate"
    }
    replicas = 1
    selector {
      match_labels = {
        "app" : "arr"
      }
    }
    template {
      metadata {
        name      = "arr"
        namespace = var.namespace
        labels = {
          "app" : "arr"
        }
      }
      spec {
        dns_config {
          option {
            name = "ndots"
            value = "1"
          }
        }
        volume {
          name = "nas-downloads"
          nfs {
            server = var.nfs_media_host
            path   = var.nfs_downloads_path
          }
        }
        volume {
          name = "nas-tv"
          nfs {
            server = var.nfs_media_host
            path   = "${var.nfs_media_path}/TV"
          }
        }
        volume {
          name = "nas-movies"
          nfs {
            server = var.nfs_media_host
            path   = "${var.nfs_media_path}/Movies"
          }
        }
        volume {
          name = "nas-music"
          nfs {
            server = var.nfs_media_host
            path   = "${var.nfs_media_path}/Music"
          }
        }
        container {
          name  = "seerr"
          image = "fallenbagel/jellyseerr:latest"
          volume_mount {
            mount_path = "/app/config"
            name       = kubernetes_persistent_volume_claim.seerr_conf.metadata[0].name
          }
          port {
            container_port = 5055
            protocol       = "TCP"
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.seerr_conf.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.seerr_conf.metadata[0].name
          }
        }
        container {
          name  = "prowlarr"
          image = "lscr.io/linuxserver/prowlarr:latest"
          port {
            container_port = 9696
            protocol       = "TCP"
          }
          volume_mount {
            name       = "prowlarr-conf"
            mount_path = "/config"
          }
        }
        volume {
          name = "prowlarr-conf"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.prowlarr_conf.metadata[0].name
          }
        }
        container {
          name  = "saytrs"
          image = "registry.gitlab.com/foxames/saytrs/saytrs:latest"
          image_pull_policy = "Always"
          volume_mount {
            name       = "saytrs-conf"
            mount_path = "/config.toml"
            sub_path   = "config.toml"
          }
          volume_mount {
            name       = "nas-tv"
            mount_path = "/tv"
          }
        }
        volume {
          name = kubernetes_config_map.saytrs_conf.metadata[0].name
          config_map {
            name = kubernetes_config_map.saytrs_conf.metadata[0].name
          }
        }
        container {
          name  = "sonarr"
          image = "linuxserver/sonarr:latest"
          port {
            container_port = 8989
            protocol       = "TCP"
          }
          volume_mount {
            name       = "sonarr-conf"
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-downloads"
            mount_path = "/downloads"
          }
          volume_mount {
            name       = "nas-tv"
            mount_path = "/tv"
          }
        }
        volume {
          name = "sonarr-conf"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.sonarr_conf.metadata[0].name
          }
        }
        container {
          name  = "radarr"
          image = "lscr.io/linuxserver/radarr:latest"
          port {
            container_port = 7878
            protocol       = "TCP"
          }
          volume_mount {
            name       = kubernetes_persistent_volume_claim.radarr_conf.metadata[0].name
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-downloads"
            mount_path = "/downloads"
          }
          volume_mount {
            name       = "nas-movies"
            mount_path = "/movies"
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.radarr_conf.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.radarr_conf.metadata[0].name
          }
        }
        container {
          name  = "lidarr"
          image = "lscr.io/linuxserver/lidarr:latest"
          port {
            container_port = 8686
            protocol       = "TCP"
          }
          volume_mount {
            name       = kubernetes_persistent_volume_claim.lidarr_conf.metadata[0].name
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-downloads"
            mount_path = "/downloads"
          }
          volume_mount {
            name       = "nas-music"
            mount_path = "/music"
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.lidarr_conf.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.lidarr_conf.metadata[0].name
          }
        }
        container {
          name  = "bazarr"
          image = "lscr.io/linuxserver/bazarr:latest"
          port {
            container_port = 6767
            protocol       = "TCP"
          }
          volume_mount {
            name       = "bazarr-conf"
            mount_path = "/config"
          }
          volume_mount {
            name       = "nas-movies"
            mount_path = "/movies"
          }
          volume_mount {
            name       = "nas-tv"
            mount_path = "/tv"
          }
        }
        volume {
          name = kubernetes_persistent_volume_claim.bazarr_conf.metadata[0].name
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.bazarr_conf.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "arr_web" {
  metadata {
    name      = "arr-web"
    namespace = var.namespace
  }
  spec {
    type = "ClusterIP"
    selector = {
      "app" : "arr"
    }
    port {
      name        = "seerr-web"
      port        = 5055
      target_port = 5055
    }
    port {
      name        = "radarr-web"
      port        = 7878
      target_port = 7878
    }
    port {
      name        = "sonarr-web"
      port        = 8989
      target_port = 8989
    }
    port {
      name        = "prowlarr-web"
      port        = 9696
      target_port = 9696
    }
    port {
      name        = "lidarr-web"
      port        = 8686
      target_port = 8686
    }
    port {
      name        = "bazarr-web"
      port        = 6767
      target_port = 6767
    }
  }
}

resource "kubernetes_manifest" "seerr_tls" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "cert-manager.io/v1"
    "kind" : "Certificate"
    "metadata" : {
      "name" : "seerr-tls"
      "namespace" : var.namespace
    }
    "spec" : {
      "secretName" : "seerr-tls"
      "duration" : "2160h0m0s"   # 90d
      "renewBefore" : "360h0m0s" # 15d
      "subject" : {
        "organizations" : [
          var.cert_organization
        ]
      }
      "commonName" : "seerr.${var.public_domain}"
      "privateKey" : {
        "algorithm" : "RSA"
        "encoding" : "PKCS1"
        "size" : "2048"
      }
      "usages" : [
        "server auth",
        "client auth"
      ]
      "dnsNames" : [
        "seerr.${var.public_domain}"
      ]
      "issuerRef" : {
        "name" : "letsencrypt-prod"
        "kind" : "ClusterIssuer"
      }
    }
  }
}

resource "kubernetes_manifest" "seerr_https_redirect" {
  manifest = {
    "apiVersion": "traefik.containo.us/v1alpha1"
    "kind": "Middleware"
    "metadata": {
      "name": "seerr-https-redirect"
      "namespace" : var.namespace
    }
    "spec": {
      "redirectScheme": {
        "scheme": "https"
        "port": "443"
        "permanent": "true"
      }
    }
  }
}

resource "kubernetes_manifest" "seerr_ingress_route" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "seerr-web"
      "namespace" : var.namespace
    }
    "spec" : {
      "entryPoints" : [
        "websecure"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`seerr.${var.public_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[0].port
            }
          ]
        },
      ]
      "tls" : {
        "secretName" : kubernetes_manifest.seerr_tls[0].manifest["spec"]["secretName"]
        "domains" : [
          {
            "main" : kubernetes_manifest.seerr_tls[0].manifest["spec"]["commonName"]
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "seerr_ingress_route_http" {
  count = var.public_domain == null ? 0 : 1
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "seerr-web-http"
      "namespace" : var.namespace
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`seerr.${var.public_domain}`)"
          "middlewares" : [
            {
              "name": kubernetes_manifest.seerr_https_redirect.manifest["metadata"]["name"]
              "namespace": kubernetes_manifest.seerr_https_redirect.manifest["metadata"]["namespace"]
            }
          ]
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[0].port
            }
          ]
        },
      ]
    }
  }
}

resource "kubernetes_manifest" "arr_ingress_route" {
  manifest = {
    "apiVersion" : "traefik.containo.us/v1alpha1"
    "kind" : "IngressRoute"
    "metadata" : {
      "name" : "arr-web"
      "namespace" : var.namespace
    }
    "spec" : {
      "entryPoints" : [
        "web"
      ]
      "routes" : [
        {
          "kind" : "Rule"
          "match" : "Host(`seerr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[0].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`radarr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[1].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`sonarr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[2].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`prowlarr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[3].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`lidarr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[4].port
            }
          ]
        },
        {
          "kind" : "Rule"
          "match" : "Host(`bazarr.${var.local_domain}`)"
          "services" : [
            {
              "name" : kubernetes_service.arr_web.metadata[0].name
              "namespace" : kubernetes_service.arr_web.metadata[0].namespace
              "passHostHeader" : "true"
              "port" : kubernetes_service.arr_web.spec[0].port[5].port
            }
          ]
        },
      ]
    }
  }
}
