variable "namespace" {
  description = "Namespace to deploy to."
  nullable    = false
  type        = string
}

variable "nfs_media_host" {
  description = "Host that provides NFS for media storage."
  nullable    = false
  type        = string
}

variable "nfs_media_path" {
  description = "Path to NFS media mount."
  nullable    = false
  type        = string
}

variable "nfs_downloads_path" {
  description = "Path to NFS downloads mount."
  nullable    = false
  type        = string
}

variable "local_domain" {
  description = "Local domain to create records under"
  nullable    = false
  type        = string
}

variable "cert_organization" {
  description = "Organization to use when registering a Let's Encrypt cert"
  nullable    = false
  type        = string
}

variable "public_domain" {
  description = "Publicly registered domain to create records under and acquire SSL certs for (optional)"
  nullable    = true
  type        = string
}

variable "wireguard_egress_config" {
  description = "Wireguard configuration file for traffic egress"
  nullable    = false
  type        = string
}

variable "sonarr_api_key" {
  description = "API key generated in Sonarr for internal services to connect with"
  nullable    = true
  type        = string
}
